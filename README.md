lv3_estpor_kod.m i lv3_estpor_sim.slx su skripta i blok dijagram za prikaz odziva
sustava sa estimacijom varijabli stanja ali bez estimacije poremecaja.
Poremecaj nastupa u petoj sekundi nakon cega se vidi da regulator ne odrzava ciljanu
amplitudu izlaza s obzirom na step pobudu (sa 0 na 0.1)

lv3_estvar_kod.m i lv3_estvar_sim.slx su skripta i blok dijagram za prikaz odziva
sustava sa estimacijom varijabli stanja i estimacijom poremecaja.
Ovakav sustav dobro kompenzira konstantnu poremecajnu velicinu i odrzava trazen 
iznos amplitude izlazne velicine (0.1)