clear all;
close all;
d1=3;
d2=2.5;
m=0.5;
k=500;
ze=0.7;
w=20;
w2=2*w;
Ts=0.02;

%polovi L
p1=exp((-1*ze*w+1i*w*sqrt(1-ze^2))*Ts);
p2=exp((-1*ze*w-1i*w*sqrt(1-ze^2))*Ts);
p3=exp(-2*ze*w*Ts);
p4=exp(-2.1*ze*w*Ts);
p5=exp(-2.2*ze*w*Ts);

%polovi K
p1e=exp((-1*ze*w2+1i*w2*sqrt(1-ze^2))*Ts);
p2e=exp((-1*ze*w2-1i*w2*sqrt(1-ze^2))*Ts);
p3e=exp(-2*ze*w2*Ts);
p4e=exp(-2.1*ze*w2*Ts);
p5e=exp(-2.2*ze*w2*Ts);

Kw=-2.2*ze*w2;



A=[0 1 0 0; -k/m -(d1+d2)/m k/m d1/m; 0 0 0 1; k/m d1/m  -k/m -(d1+d2)/m];
B=[0; 1/m; 0; 0];
C=[0 0 1 0];
D=[0];


[a,b,c,d]=c2dm(A,B,C,D,Ts);

L=place(a,b,[p1 p2 p3 p4]);
K=place(a',c',[p1e p2e p3e p4e]);
fi=a-b*L;
[num,den]=ss2tf(fi,b,c,d);
sys=tf(num,den,Ts);
k=1/(sum(num)/sum(den));

open 'lv3_estvar_adamcevic'
sim 'lv3_estvar_adamcevic'
figure
plot(y, 'b')
hold on
plot(y_est, 'r')
legend('Mjeren izlaz', 'Estimiran izlaz')

